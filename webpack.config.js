var path = require('path')
var webpack = require('webpack')

module.exports = {
  context: __dirname,
  devtool: 'inline-source-map',
  entry: [
    './App.js'
  ],
  output: {
    path: path.join(__dirname, 'build'),
    filename: 'bundle.js',
    publicPath: '/static/'
  },
  module: {
    loaders: [{
        test: /\.js$/,
        loaders: ['babel-loader'],
        exclude: /node_modules/
      }
    ]
  },
}
